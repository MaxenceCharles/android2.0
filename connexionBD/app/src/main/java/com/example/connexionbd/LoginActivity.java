package com.example.connexionbd;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginActivity extends AppCompatActivity {

    EditText monEmail, monmdp;
    Button monbtn;
    TextView maConnexion;
    FirebaseAuth myFireBaseAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        myFireBaseAuth = FirebaseAuth.getInstance();
        monEmail = findViewById(R.id.emailId);
        monmdp = findViewById(R.id.password);
        monbtn = findViewById(R.id.btn);
        maConnexion = findViewById(R.id.inscrip);
        monbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = monEmail.getText().toString();
                String pwd = monmdp.getText().toString();
                if(email.isEmpty()){
                    monEmail.setError("Entrez un email valide");
                    monEmail.requestFocus();
                }
                else if(pwd.isEmpty()){
                    monmdp.setError("Entrez le bon mot de passe");
                    monmdp.requestFocus();
                }
                else if(email.isEmpty() && pwd.isEmpty()){
                    Toast.makeText(getApplicationContext(), "les champs sont incorrects", Toast.LENGTH_SHORT);
                }
                else if(!(email.isEmpty() && pwd.isEmpty())){
                    myFireBaseAuth.signInWithEmailAndPassword(email,pwd).addOnCompleteListener(LoginActivity.this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(!task.isSuccessful()){
                                Toast.makeText(getApplicationContext(), "connexion échouée, veuillez réessayer", Toast.LENGTH_SHORT);
                            }
                            else{
                                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                            }
                        }
                    });
                }
                else{
                    Toast.makeText(getApplicationContext(), "connexion réussie", Toast.LENGTH_SHORT);
                }
            }
        });

        maConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), InscriptionActivity.class);
                startActivity(i);
            }
        });
    }
}
