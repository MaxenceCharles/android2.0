package com.example.connexionbd.utils.drawer;

/**
 * Classe représentant des ressource drawable d'android utilisé pour être rendue par un {@link Drawer}
 * @version 1.0.0
 */
public final class AndroidDrawableResource extends com.example.connexionbd.utils.drawer.DrawableResource<Integer> {

    /**
     * Créer une ressource android
     * @param ressourceId Identifiant android de la ressource
     */
    public AndroidDrawableResource(int ressourceId) {
        super (ressourceId);
    }

}
