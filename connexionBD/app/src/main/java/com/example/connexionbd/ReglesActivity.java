package com.example.connexionbd;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ReglesActivity extends AppCompatActivity {

    Button btnRetourMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regles);
        btnRetourMenu = findViewById(R.id.retourmenu);

        btnRetourMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToMenu = new Intent(ReglesActivity.this, HomeActivity.class);
                startActivity(intToMenu);
            }
        });
    }


}
