package com.example.connexionbd;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class InscriptionActivity extends AppCompatActivity {

    EditText monEmail, monmdp, monNom, monPrenom, monSexe, monAge;
    Button monboutonAInscription;
    TextView maConnect;
    FirebaseAuth myFireBaseAuth;
    DatabaseReference reff;
    //private FirebaseAuth.AuthStateListener monAuthStateListener;
    Joueur joueur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inscription);

        monAge = findViewById(R.id.Ageid);
        myFireBaseAuth = FirebaseAuth.getInstance();
        monNom = findViewById(R.id.nom);
        monPrenom = findViewById(R.id.prenom);
        monEmail = findViewById(R.id.Email);
        monSexe = findViewById(R.id.sexeid);
        maConnect = findViewById(R.id.connect);
        monmdp = findViewById(R.id.MDP);
        //joueur = new Joueur();
        reff = FirebaseDatabase.getInstance().getReference().child("Joueurs");
       /* reff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot joueurSnapshot: dataSnapshot.getChildren()){
                    Log.i(Tag, joueurSnapshot.child("joueur").getValue(String.class));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.w(Tag, "onCancellede", databaseError.toException());
            }
        });*/
        monboutonAInscription = findViewById(R.id.btnAincription);



        monboutonAInscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = monEmail.getText().toString();
                String pwd = monmdp.getText().toString();
                int age =Integer.parseInt(monAge.getText().toString().trim());
                String sexeJ = monSexe.getText().toString().trim();
                String prenom = monPrenom.getText().toString().trim();
                String nom = monNom.getText().toString().trim();

                joueur.setNom(nom);
                joueur.setPrenom(prenom);
                joueur.setAge(age);
                joueur.setSexe(sexeJ);
                joueur.setEmail(email);
                joueur.setMotDePasse(pwd);

                reff.push().setValue(joueur);
                Toast.makeText(InscriptionActivity.this, "inscription réussie", Toast.LENGTH_LONG).show();


            }

        });

        maConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent retourConnexion = new Intent(InscriptionActivity.this, LoginActivity.class);
                startActivity(retourConnexion);
            }
        });

    }

}