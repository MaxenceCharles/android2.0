package com.example.connexionbd;


import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

/**
 * @version 1.0.0
 */


public class Game {
    public boolean enable = true;

    public void toggleButton() {
        enable = !enable;
        battleActivity.imageCiseaux.setClickable(enable);
        battleActivity.imageCiseaux.setEnabled(enable);
        if (!enable) battleActivity.imageCiseaux.setAlpha(0.3f);
        else battleActivity.imageCiseaux.setAlpha(1f);

        battleActivity.imagePierre.setClickable(enable);
        battleActivity.imagePierre.setEnabled(enable);
        if (!enable) battleActivity.imagePierre.setAlpha(0.3f);
        else battleActivity.imagePierre.setAlpha(1f);

        battleActivity.imageFeuille.setClickable(enable);
        battleActivity.imageFeuille.setEnabled(enable);
        if (!enable) battleActivity.imageFeuille.setAlpha(0.3f);
        else battleActivity.imageFeuille.setAlpha(1f);

        battleActivity.imageSpoke.setClickable(enable);
        battleActivity.imageSpoke.setEnabled(enable);
        if (!enable) battleActivity.imageSpoke.setAlpha(0.3f);
        else battleActivity.imageSpoke.setAlpha(1f);

        battleActivity.imageLezard.setClickable(enable);
        battleActivity.imageLezard.setEnabled(enable);
        if (!enable) battleActivity.imageLezard.setAlpha(0.3f);
        else battleActivity.imageLezard.setAlpha(1f);


    }

    public void resetChoice() {
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                playerChoice = Choice.UNSET;
                advChoice = Choice.UNSET;
                toggleButton();
                battleActivity.updateUi();
            }
        }, 2000);
    }

    public enum GameType{
        SINGLE_PLAYER,
        MULTI_PLAYER
    }


    @NonNull
    private final battleActivity battleActivity;

    /**
     * Choix de l'utilisateur
     */
    @NonNull
    private Choice playerChoice = Choice.UNSET;

    /**
     * Choix de l'adversaire
     */
    @NonNull
    private Choice advChoice = Choice.UNSET;

    /**
     * Score du joueur
     */
    private int playerScore = 0;

    /**
     * Score de l'adversaire
     */
    private int advScore = 0;


    /**
     * Créer un jeu
     * @param battleActivity Activité
     */
    public Game(@NonNull battleActivity battleActivity) {
        this.battleActivity= battleActivity;
    }

    /**
     * Met à jours les score en fonction des choix
     */
    public void updateScore() {
        if (this.playerChoice.win(this.advChoice))
            this.playerScore++;
        else if(this.advChoice.win(this.playerChoice))
            this.advScore++;
    }

    public void resetScore() {
        this.playerScore = 0;
        this.advScore = 0;
    }

    @NonNull
    public Choice getPlayerChoice() {
        return playerChoice;
    }

    public void setPlayerChoice(@NonNull Choice playerChoice) {
        this.playerChoice = playerChoice;
    }

    @NonNull
    public Choice getAdvChoice() {
        return advChoice;
    }

    public void setAdvChoice(@NonNull Choice advChoice) {
        this.advChoice = advChoice;
    }


    public int getPlayerScore() {
        return playerScore;
    }

    @NonNull
    public battleActivity getBattleActivity(){
        return battleActivity;
    }
}
