package com.example.connexionbd;
import androidx.appcompat.app.AppCompatActivity;

public abstract class Joueur extends AppCompatActivity{

    private static String userId;


    private int id;
    private String nom;
    private String prenom;
    private Integer age;
    private String Sexe;
    private String email;
    private String MotDePasse;

    public Joueur() {
    }

    public static String getUserId() {
        return userId;
    }

    public static void setUserId(String userId) {
        Joueur.userId = userId;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getSexe() {
        return Sexe;
    }

    public void setSexe(String sexe) {
        Sexe = sexe;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMotDePasse() {
        return MotDePasse;
    }

    public void setMotDePasse(String motDePasse) {
        MotDePasse = motDePasse;
    }
}
