package com.example.connexionbd;

import android.annotation.*;

import androidx.annotation.NonNull;

import com.example.connexionbd.R;
import com.example.connexionbd.utils.drawer.AndroidDrawableResource;
import com.example.connexionbd.utils.drawer.DrawableResource;
import java.util.Random;


public enum Choice {
    UNSET  (new AndroidDrawableResource(R.drawable.hourglass)),
    SPOKE (new AndroidDrawableResource(R.drawable.spoke)),
    LEZARD (new AndroidDrawableResource(R.drawable.lezard)),
    PIERRE(new AndroidDrawableResource(R.drawable.pierre)),
    FEUILLE(new AndroidDrawableResource(R.drawable.feuille)),
    CISEAUX(new AndroidDrawableResource(R.drawable.ciseaux));

    /**
     * Objet permetant de gérer l'aléatoire
     */
    private static final Random random = new Random();

    /**
     * Effectue un choix aléatoire parmis les différents choix possible
     * @return Un choix aléatoire
     */
    @NonNull
    public static Choice randomChoice() {
        Choice choice;
        do {
            choice = Choice.values()[random.nextInt(Choice.values().length)];
        }while (choice.equals(UNSET));
        return choice;
    }

    /**
     * L"identifiant du drawable utilisé pour représenter le choix
     */

    public final DrawableResource ressource;

    /**
     * Constructeur d'un choix
     * @param ressource Ressource utilisé pour être rendue
     */
    Choice(DrawableResource ressource) {
        this.ressource = ressource;
    }

    /**
     * Test si le choix passé en paramètre est battus par le choix actuel
     * @param adv Le choix à tester
     * @return Vraie si adv est battu par le le choix actuel, faux sinon ou si exéquo
     */
    public boolean win(Choice adv) {
        switch (adv) {
            case PIERRE: return this.equals(FEUILLE);
            //case PIERRE: return this.equals(SPOKE);
            case CISEAUX: return this.equals(PIERRE);
            //case CISEAUX: return this.equals(SPOKE);
            case FEUILLE: return this.equals(CISEAUX);
            //case FEUILLE: return this.equals(LEZARD);
            case SPOKE: return this.equals(LEZARD);
            //case SPOKE: return this.equals(FEUILLE);
            case LEZARD: return this.equals(CISEAUX);
            //case LEZARD: return this.equals(PIERRE);

            default: return false;
        }
    }

    /**
     * Obtiens la ressource utilisé pour le rendu
     * @return La ressource représentant le choix
     */
    public DrawableResource getRessource() {
        return ressource;
    }
}
