package com.example.connexionbd.utils.opponent;

import android.annotation.*;

import androidx.annotation.NonNull;

import com.example.connexionbd.Game;
import com.example.connexionbd.utils.opponent.ComputerOpponent;
import com.example.connexionbd.utils.opponent.Opponent;
import com.example.connexionbd.utils.opponent.PlayerOpponent;

public final class OpponentFactory {

    public static @NonNull
    Opponent getOpponent(final Game.GameType gameType) {
        switch (gameType) {
            case MULTI_PLAYER: return new PlayerOpponent();
            case SINGLE_PLAYER: default: return new ComputerOpponent();
        }
    }
}
