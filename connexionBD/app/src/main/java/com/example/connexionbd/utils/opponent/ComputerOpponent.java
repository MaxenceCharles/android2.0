package com.example.connexionbd.utils.opponent;

import com.example.connexionbd.Choice;

public class ComputerOpponent implements Opponent {

    @Override
    public Choice getChoice() {
        return Choice.randomChoice();
    }
}
