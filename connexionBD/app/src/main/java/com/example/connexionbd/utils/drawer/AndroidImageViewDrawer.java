package com.example.connexionbd.utils.drawer;



import androidx.appcompat.widget.AppCompatImageView;

/**
 * Classe représentant une {@link AndroidImageViewDrawer} qui peut rendre un objet de type {@link AndroidDrawableResource}
 * @version 1.0.0
 */
public final class AndroidImageViewDrawer  implements Drawer<AndroidDrawableResource> {


    private final AppCompatImageView imageView;

    public AndroidImageViewDrawer(AppCompatImageView imageView) {
        this.imageView = imageView;
    }


    @Override
    public void drawResource(AndroidDrawableResource resource) {
        this.imageView.setImageResource(resource.getResource());
    }
}
