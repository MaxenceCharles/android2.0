package com.example.connexionbd;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class JeuActivity extends Joueur {

    private DatabaseReference userDatabaseReference = FirebaseDatabase.getInstance().getReference().child("joueur");
    private FirebaseAuth monAuth = FirebaseAuth.getInstance();
    private FirebaseUser user = monAuth.getCurrentUser();
    private TextView BestScore;
    private Button btnRetour;
    private Button btnGame;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu);

        btnRetour = findViewById(R.id.backbtn);

        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), HomeActivity.class));
            }
        });

        btnGame = findViewById(R.id.startingbtn);
        btnGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), battleActivity.class));
            }
        });

        BestScore = findViewById(R.id.best_score_tv);

        DisplayScore();

    }
    private void DisplayScore(){
        try{
            userDatabaseReference.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    try{
                        final String score = dataSnapshot.child("score").getValue().toString();
                        BestScore.setText("votre score ="+ score);
                    }catch(NullPointerException ex){
                        ex.printStackTrace();
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }catch(Exception ex){
            Toast.makeText(getApplicationContext(), ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}
