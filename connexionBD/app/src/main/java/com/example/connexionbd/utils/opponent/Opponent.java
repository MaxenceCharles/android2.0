package com.example.connexionbd.utils.opponent;

import com.example.connexionbd.Choice;

public interface Opponent {
    public Choice getChoice();
}
