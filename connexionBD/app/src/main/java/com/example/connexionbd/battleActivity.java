package com.example.connexionbd;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.example.connexionbd.Game;
import com.example.connexionbd.Choice;



import com.example.connexionbd.utils.opponent.Opponent;
import com.example.connexionbd.utils.opponent.OpponentFactory;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.drawerlayout.widget.DrawerLayout;

import com.example.connexionbd.utils.drawer.AndroidDrawableResource;
import com.example.connexionbd.utils.drawer.AndroidImageViewDrawer;
import com.example.connexionbd.utils.drawer.DrawableResource;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

public class battleActivity extends AppCompatActivity {

    @NonNull
    private final Game.GameType GAME_TYPE = Game.GameType.MULTI_PLAYER;

    @NonNull
    private final Game game = new Game(this);
    private final Opponent opponent = OpponentFactory.getOpponent(GAME_TYPE);





    private AndroidImageViewDrawer imgAdversaire;
    private AndroidImageViewDrawer ImageJoueur;

    @SuppressWarnings("WeakerAccess")
    AppCompatImageView imgadv;

    @SuppressWarnings("WeakerAccess")
    AppCompatImageView imgNous;

    ImageButton imagePierre;
    ImageButton imageFeuille;
    ImageButton imageCiseaux;
    ImageButton imageSpoke;
    ImageButton imageLezard;
    TextView txt_your_score, tkt_score_adv;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battle);
        imgAdversaire = new AndroidImageViewDrawer(imgadv);
        ImageJoueur = new AndroidImageViewDrawer(imgNous);
        imagePierre = findViewById(R.id.imgbtnPierre);
        imageFeuille = findViewById(R.id.imgbtnCiseaux);
        imageCiseaux = findViewById(R.id.imgbtnFeuille);
        imageLezard = findViewById(R.id.imgbtnLezard);
        imageSpoke = findViewById(R.id.imgbtnSpoke);
        tkt_score_adv = findViewById(R.id.scoreAdv);
        txt_your_score = findViewById(R.id.scoreNous);


        //callback du clic sur le btn ciseaux
        imageCiseaux.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                game.setPlayerChoice(Choice.CISEAUX);
                //game.setAdvChoice(oponent.getChoice());
                //game.updateScore();
                updateUi();
            }
        });
        //callback du clic sur le btn pierre
        imagePierre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                game.setPlayerChoice(Choice.PIERRE);
                //game.getAdvChoice(oponent.getChoice());
                //game.updateScore();
                updateUi();
            }
        });
        //callback du clic sur le btn feuille
        imageFeuille.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                game.setPlayerChoice(Choice.FEUILLE);
               // game.getAdvChoice(oponent.getChoice());
                //game.updateScore();
                updateUi();
            }
        });
        //callback du clic sur le btn spoke
        imageSpoke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                game.setPlayerChoice(Choice.SPOKE);
               // game.getAdvChoice(oponent.getChoice());
               // game.updateScore();
                updateUi();
            }
        });
        //callback du clic sur le btn lezard
        imageLezard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                game.setPlayerChoice(Choice.LEZARD);
               // game.getAdvChoice(oponent.getChoice());
               // game.updateScore();
                updateUi();
            }
        });




    }


    public void updateUi(){
        synchronized (this){
            //ImageJoueur.drawResource((AndroidDrawableResource)game.getPlayerChoice().getRessource());
            //imgAdversaire.drawResource((AndroidDrawableResource) game.getAdvChoice().getRessource());
            StringBuilder strB_adv = new StringBuilder();
            StringBuilder strB_you = new StringBuilder();

            strB_adv.append("score adversaire");
            strB_adv.append(this.game.getPlayerScore());
            this.tkt_score_adv.setText(strB_adv);


            strB_you.append("votre score");
            strB_you.append(this.game.getPlayerScore());
            this.txt_your_score.setText(strB_you);

        }
    }
}
