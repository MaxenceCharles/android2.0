package com.example.connexionbd;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

public class HomeActivity extends AppCompatActivity {

    Button btnDeco;
    Button btnjouer;
    Button btnregles;
    FirebaseAuth myFirebaseAuth;
    private FirebaseAuth.AuthStateListener monAuthStateListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        btnDeco = findViewById(R.id.logout);
        btnjouer = findViewById(R.id.play);
        btnregles = findViewById(R.id.rules);


        btnDeco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intToLogin = new Intent(HomeActivity.this, LoginActivity.class);
                startActivity(intToLogin);
            }
        });

        btnregles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToRules = new Intent(HomeActivity.this, ReglesActivity.class);
                startActivity(intToRules);
            }
        });

        btnjouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intToPlay = new Intent(HomeActivity.this, JeuActivity.class);
                startActivity(intToPlay);
            }
        });
    }

}
